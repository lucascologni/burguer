package com.burguer.model.dao.impl;

import static org.mockito.Mockito.when;

import org.junit.Test;
import org.mockito.Mockito;

import com.burguer.model.Person;
import com.burguer.model.dao.LoginDAO;

public class LoginDAOImplTests {

	private LoginDAO loginDAO;
	private Person person;

	@Test
	public void testAuthenticate() {

		person = new Person();

		person.getLogin().setUsername("admin");
		person.getLogin().setUsername("admin123");

		loginDAO = Mockito.mock(LoginDAO.class);

		try {
			when(loginDAO.authenticate(person)).thenReturn(person);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}