package com.burguer.model.dao;

import com.burguer.model.Person;

// DAO - Data Access Object - Interface
public interface LoginDAO {
	
	public Person authenticate(Person person) throws Exception;

}