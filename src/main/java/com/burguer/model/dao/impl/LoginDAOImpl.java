package com.burguer.model.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.burguer.model.Person;
import com.burguer.model.dao.LoginDAO;
import com.burguer.util.DynamicSingleton;

// DAO - Data Access Object - Implementation - All the communication with database are located here.
@Repository
public class LoginDAOImpl implements LoginDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	@Transactional(readOnly = true)
	public Person authenticate(Person person) throws Exception {

		return this.getJdbcTemplate()
				.query("select p.profile from login l, person p where p.login_id = l.id and l.username = '"
						+ person.getLogin().getUsername() + "' and l.password = '" + person.getLogin().getPassword()
						+ "'", new PersonRowMapper())
				.get(0); // This query only returns a single row, then I get the index 0.
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	// Inner Class - Map a table person row returned from database.
	class PersonRowMapper implements RowMapper<Person> {

		private Person person;

		@Override
		public Person mapRow(ResultSet rs, int rowNum) throws SQLException {

			this.setPerson((Person) DynamicSingleton.getInstance(Person.class));

			this.getPerson().setProfile(rs.getString("profile"));

			return this.getPerson();
		}

		public Person getPerson() {
			return person;
		}

		public void setPerson(Person person) {
			this.person = person;
		}
	}
}