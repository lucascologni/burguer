package com.burguer.controller;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.burguer.controller.bcl.LoginBCL;
import com.burguer.model.Person;
import com.burguer.util.DynamicSingleton;

@Controller
public class LoginController {

	// Variables
	private static final String ADMIN = "A";
	private static final String CUSTOMER = "C";
	
	private Person person;
	private String view;

	@Autowired
	private LoginBCL loginBCL;

	// Methods

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String login(Model model) {

		try {
			// I used a singleton, to avoid multiple customer instantiation.
			this.setPerson((Person) DynamicSingleton.getInstance(Person.class));

			// I pass the object "customer" to an attribute to use in my view page.
			model.addAttribute("person", this.getPerson());
			this.setView("Login");

		} catch (Exception e) {
			Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null,
					"Erro ao retornar a view Login. " + e);
		}

		// I return the "Login.html" view page.
		return this.getView();
	}

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public String authenticate(@ModelAttribute Person person, Model model) {

		this.getPerson().getLogin().setUsername(person.getLogin().getUsername());
		this.getPerson().getLogin().setPassword(person.getLogin().getPassword());

		try {
			this.setPerson(this.getLoginBCL().authenticate(this.getPerson()));

			// If the person profile (A - Admin - RequestManagement | C - Customer - SnackRequest)
			if (this.getPerson().getProfile().equalsIgnoreCase(ADMIN)) {
				this.setView("RequestManagement");

			} else if (this.getPerson().getProfile().equalsIgnoreCase(CUSTOMER)) {
				this.setView("SnackRequest");

			} else {
				this.setView("error/401");
			}
		} catch (Exception e) {
			Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null,
					"Login incorreto ou inexistente. " + e);

			this.setView("error/401");
		}

		return this.getView();
	}

	// Properties
	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public LoginBCL getLoginBCL() {
		return loginBCL;
	}

	public String getView() {
		return view;
	}

	public void setView(String view) {
		this.view = view;
	}

	public void setLoginBCL(LoginBCL loginBCL) {
		this.loginBCL = loginBCL;
	}
}