package com.burguer.controller.bcl.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.burguer.controller.bcl.LoginBCL;
import com.burguer.model.Person;
import com.burguer.model.dao.LoginDAO;

// BCL - Business Control Logic - Implementation - All the business logic are located here.
@Service
public class LoginBCLImpl implements LoginBCL {

	@Autowired
	private LoginDAO loginDAO;

	@Override
	public Person authenticate(Person person) throws Exception {
		return this.getLoginDAO().authenticate(person);
	}

	public LoginDAO getLoginDAO() {
		return loginDAO;
	}

	public void setLoginDAO(LoginDAO loginDAO) {
		this.loginDAO = loginDAO;
	}
}