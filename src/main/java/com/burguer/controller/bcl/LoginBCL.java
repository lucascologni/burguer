package com.burguer.controller.bcl;

import com.burguer.model.Person;

// BCL - Business Control Logic - Interface
public interface LoginBCL {

	public Person authenticate(Person person) throws Exception;
}