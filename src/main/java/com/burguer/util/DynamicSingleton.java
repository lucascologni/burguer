package com.burguer.util;

public class DynamicSingleton {

	private static Object _tn;

	@SuppressWarnings("unchecked")
	public static <T> T getInstance(Class<T> t) {
		
		if (_tn != null) {
			return (T) _tn;
		} try {
			_tn = t.newInstance();
			;
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return (T) _tn;
	}
}