package com.burguer.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = { "com.burguer" })
public class StartBurguerApplication {

	public static void main(String[] args) {
		SpringApplication.run(StartBurguerApplication.class, args);
	}
}