create table login(
id int not null primary key,
username varchar2(20) not null,
password varchar2(20) not null);

create table person(
id int not null primary key,
name varchar2(50) not null,
address varchar2(200) not null,
profile char(1) not null, -- C-Customer, A-Admin
login_id int not null,
constraint fk_login_id foreign key (login_id) references login (id));

create table snack(
id int not null primary key,
name varchar2(50),
type_of_bread varchar2(50) not null,
cheese varchar2(50),
filling varchar2(50),
salad varchar2(50) not null,
sauces varchar2(50),
spice varchar2(50),
snack_value decimal(3,2) not null);

create table request(
id int not null primary key, -- Request number
person_id int not null,
snack_id int not null,
request_value decimal(3,2) not null,
date_time date not null,
constraint fk_person_id foreign key (person_id) references person (id),
constraint fk_snack_id foreign key (snack_id) references snack (id));