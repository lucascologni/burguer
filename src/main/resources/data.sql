insert into login(id, username, password) values(1, 'admin', 'admin123');
insert into person(id, name, address, profile, login_id) values(1, 'Lucas', 'R. Castro Silva', 'A', 1); -- Admin

insert into login(id, username, password) values(2, 'custom', 'custom123');
insert into person(id, name, address, profile, login_id) values(2, 'Victor', 'R. Cesario Motta', 'C', 2); -- Customer