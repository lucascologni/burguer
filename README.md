# Tutorial - Project Installation and Configuration #
 
**1º** Download the GIT tool, used for version control of project:
 
* [Git](https://git-scm.com/downloads)
 
**2º** After Git installation, open the Windows command prompt or Ubuntu terminal (Lynux) and perform the repository clone:
 
```bash
git clone https://lucascologni@bitbucket.org/lucascologni/burguer.git
```

**3º** Download the Oracle Database Express Edition 11g Release 2:

* [Oracle Database Express Edition 11g Release 2](http://www.oracle.com/technetwork/database/database-technologies/express-edition/downloads/index.html)

**4º** Install the database and set a password for the main user (system).

**5º** After the database installation, open the file "Run SQL Command Line", do the log in with the username and password of the database installation:

```bash
SQL> connect
Enter user-name: system
Enter password:  your-password
Connected.
```

**6º** Create the database user used in the Burger project:

```bash
SQL> create user burguer identified by website;
User created.
```

**7º** Give these DBA permissions for the created user:

```bash
SQL> grant connect, resource, dba to burguer;
Grant succeeded.
```

**8º** Download the Gradle Tool, used for project compilation automation:

* [Gradle v3.4.1](https://services.gradle.org/distributions/gradle-3.4.1-bin.zip)
 
**9º** Follow the tutorial bellow to install the Gradle tool on Windows:

* [Tutorial - Gradle Installation - Windows](https://rominirani.com/gradle-tutorial-part-1-installation-setup-2ea77729fc8c)
 
**10º** After Gradle installation, open the project directory with Windows command prompt or the Ubuntu terminal.
 
**11º** Type the commands bellow to do the project build:
 
```bash
gradle build
```
 
```bash
gradle bootRun
```
**12º** Access the project through the browser with the following URL:
 
* [Burguer](http://localhost:1991)
 
**13º** Log in with the pre-registered users in the database:
 
Administrator - Access the Order Management page:
 
```bash
username: admin
password: admin123
```
 
Client - Access the snack order page:
 
```bash
username: custom
password: custom123
```
 
OBS: Table creation and data insertion script files are located in the following directory:
 
```bash
Burguer\src\main\resources\data.sql
```

```bash
Burguer\src\main\resources\schema.sql
```
 
Schema Access User in Oracle Database:

```bash
username: burguer
password: website
```

# Open source tools used in the project development #
 
### Version Control: ###
 
* Git
 
### Compilation Automation: ###
 
* Gradle
 
### Integrated Development Environment (IDE): ###
 
* Eclipse Neon 3
* Oracle SQL Developer
 
### Programming Languages: ###
 
* Front-end: HTML5, CSS3, Javascript
* Back-end: Java EE, SQL
 
### Frameworks: ###
 
* Front-end: Thymeleaf
* Back-end: MVC, Spring Boot, Spring Framework, Spring JDBC
* Tests: JUnit, Mockito, Hamcrest
 
### Design Patterns: ###
 
* Singleton
 
### Database: ###
 
* Oracle Database Express Edition 11g Release 2